package i.m.a.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.xml.bind.DatatypeConverter;

public class Main {

	public static String valuestring = "eyJ0aW1lc3RhbXAiOjE0MTgzMjY2NDM3MjcsInByb2ZpbGVJZCI6IjQwYTE2MDdlOTYzZjQyMDViYzU0ZjBkNTI5MzM0MzI4IiwicHJvZmlsZU5hbWUiOiJwb2RwYWdlIiwiaXNQdWJsaWMiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8yMjkzNTNhOWU1NWM1NGEzYjk4MTVjOGI5Nzc5N2NiYWViNjViYTYzNmEzYmU5Njc1ZWQ1ODQyNjQ1MTUifSwiQ0FQRSI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzhhZGI0NWRiZWZlMjQ2NTc0NWE2ZDFmY2VlOWI4YTcyMTc5MmUyMWM1ZWIxNTI0ZWY1MTZiYjZmYjE4OSJ9fX0=";
	public static String signaturestring = "DmCfMqodJ1XAx32p1+9Tp5uiZOw7T1uEzwiEwyB9UZYyiQK3n1A2MSlJZC2Auaw7cQ3AIumDhzkoKJPUnkN4muK1NoLO3R/E3etQj2ffuJDXxVKzr5Ex/ryYpyv6zw8Rt0ZMRsD0CMHNQQsGMpC4RazhbdIPeZcmj/lMOkT0HAtfrVpzI9fTSHw5FU9ZRWACfS9arza0mN+e+L+a1eOJ33aGTPjEsMdd/RtPZ7aiWYQQ198mPsdRwdm+XBQdKT+a1VGhgbK3MB56iVjVDciT74Tla2I+ULOLqQawT1/f06T+QB1apTj2P5wrGe/0nCBTVatYeNyL6ztLj9qSuTweXTX9Gn3+xJe6IVkrmgT1BoMon2uh2MkMq35h6nqK5biB/QgxPKgBUPdiIW2CSBQprOS3XS6fN5qBuoah//DaO9ukSOewZma7uMo9e8aDIboGIAQk34q98z+vJkVS8JLJI5E3A/UMR72eRDfPiZ8+FuGVSRrU7SQES7YtGpvjby0XHesoNuLYMwMRorKtAYrvzoZAeiL/m+3Xmv9HXn78nyty0zwQNfWitKZt8ROZrSiDaKmc/SV8YVh+GphdHnxT1VTZ1hCWBvF/dxdcS3mvb6g+gm2TVxyZZSaPRkLUmVortBvJKLvSpwS/s523IzcyglEpR/ZcR97+bHlpBxjaLEo=";
	public static PublicKey publicKey;

	public static void main(String[] args) throws InvalidKeySpecException, IOException, NoSuchAlgorithmException {
		
		X509EncodedKeySpec spec = new X509EncodedKeySpec(toByteArray(Main.class.getResourceAsStream("/yggdrasil_session_pubkey.der"))); //Hohlt den PublicKey
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		
		publicKey = keyFactory.generatePublic(spec);
		System.out.println(new String(spec.getEncoded()));
		System.out.println(isSignatureValid(publicKey));
	}

	public static boolean isSignatureValid(PublicKey publicKey) {
		try {
			Signature signature = Signature.getInstance("SHA1withRSA");
			signature.initVerify(publicKey);
			signature.update(valuestring.getBytes());
			return signature.verify(decodeBase64(signaturestring));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static byte[] decodeBase64(String s) {
		return DatatypeConverter.parseBase64Binary(s);
	}

	public static byte[] toByteArray(InputStream input) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		copy(input, output);
		return output.toByteArray();
	}

	public static int copy(InputStream input, OutputStream output) throws IOException {
		long count = copyLarge(input, output);
		if (count > 2147483647L) {
			return -1;
		}
		return (int) count;
	}

	public static long copyLarge(InputStream input, OutputStream output) throws IOException {
		return copyLarge(input, output, new byte[4096]);
	}

	public static long copyLarge(InputStream input, OutputStream output, byte[] buffer) throws IOException {
		long count = 0L;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}
}
